package sizuha.library.slib.types;

import sizuha.library.slib.squery.DBRowExchanger;

public interface DBRowExchangerCreator {

    DBRowExchanger create();

}
