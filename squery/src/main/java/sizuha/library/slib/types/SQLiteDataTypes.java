package sizuha.library.slib.types;

public enum SQLiteDataTypes {

    Text(10),
    Integer(20),
    Real(30),
    Blob(40),
    DateTime(11),
    Date(12),
    Time(13),
    Timestemp(21),
    Bool(22);

    private int value;

    SQLiteDataTypes(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        switch (value) {
            case 20:
            case 21:
            case 22:
                return "INTEGER";

            case 2: return "REAL";
            case 3: return "BLOB";
            default: return "TEXT";
        }
    }

    @FunctionalInterface
    public interface EachAction<TItem> {
        void execute(TItem item);
    }


}
