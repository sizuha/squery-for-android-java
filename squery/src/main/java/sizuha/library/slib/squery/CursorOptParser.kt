package sizuha.library.slib.squery

import android.database.Cursor
import java.text.DateFormat
import java.text.ParseException
import java.util.*

class CursorOptParser(private val cursor: Cursor?) {

    fun getString(column: String) = parseString(cursor, column)
    fun getInt(column: String) = parseInt(cursor, column)
    fun getLong(column: String) = parseLong(cursor, column)
    fun getFloat(column: String) = parseFloat(cursor, column)
    fun getDouble(column: String) = parseDouble(cursor, column)

    fun getBoolean(column: String) = parseBoolean(cursor, column)
    fun getDateFromLong(column: String) = parseTimestamp(cursor, column)

}


fun checkCursorValid(cursor: Cursor?, column: String): Boolean {
    val idx = cursor?.getColumnIndex(column) ?: return false
    return checkCursorValid(cursor, idx)
}

fun checkCursorValid(cursor: Cursor?, idx: Int): Boolean {
    cursor?.let {
        if (!it.isClosed) {
            return idx >= 0 && !cursor.isNull(idx)
        }
    }
    return false
}


fun parseString(cursor: Cursor?, column: String): String? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, idx)) return null

    return cursor.getString(idx)
}


fun parseInt(cursor: Cursor?, column: String): Int? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, idx)) return null

    return cursor.getInt(idx)
}


fun parseBoolean(cursor: Cursor?, column: String): Boolean? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, column)) return null

    return cursor.getInt(idx) != 0
}


fun parseLong(cursor: Cursor?, column: String): Long? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, column)) return null

    return cursor.getLong(idx)
}

fun parseFloat(cursor: Cursor?, column: String): Float? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, column)) return null

    return cursor.getFloat(idx)
}

fun parseDouble(cursor: Cursor?, column: String): Double? {
    val idx = cursor?.getColumnIndex(column) ?: return null
    if (!checkCursorValid(cursor, column)) return null

    return cursor.getDouble(idx)
}

fun parseTimestamp(cursor: Cursor?, column: String): Date? {
    val value = parseLong(cursor, column)
    return if (value != null) Date(value) else null
}

fun parseDateTimeFormat(cursor: Cursor?, column: String, format: DateFormat): Date? {
    val value = parseString(cursor, column)
    if (value != null) {
        return try {
            format.parse(value)
        }
        catch (e: ParseException) {
            null
        }
    }

    return null
}