package sizuha.library.slib.squery;


import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;

import sizuha.library.slib.types.DBRowExchangerCreator;
import sizuha.library.slib.types.SQLiteDataTypes;


final public class SQuery {

    static final String TAG = "SQuery";

    private static boolean debugLogEnabled = false;
    public static boolean isDebugLogEnabled() { return debugLogEnabled; }

    public static void setDebugLogEnable(boolean flag) {
        debugLogEnabled = flag;
    }

    private DatabaseOpenHelper m_dbOpenHelper;
    private SQLiteDatabase m_db;

    public SQuery(DatabaseOpenHelper openDatabaseOpenHelper) {
        this.m_dbOpenHelper = openDatabaseOpenHelper;
        this.m_db = openDatabaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        m_db = null;

        if (m_dbOpenHelper != null) m_dbOpenHelper.close();
        m_dbOpenHelper = null;
    }

    public boolean isClosed() {
        return m_db == null || m_dbOpenHelper == null;
    }

    public void excuteSql(String sql, Object... args) throws SQLException {
        m_db.execSQL(sql, args);
    }

    public Cursor rawQuery(String sql, String... args) {
        return m_db.rawQuery(sql, args);
    }

    public SQLiteDatabase getRawDB() { return m_db; }

    public QueryBuilder from(TableDefine table) {
        final QueryBuilder sql = new QueryBuilder(m_db, table);
        return sql;
    }

    public static class CursorParser {
        private Cursor m_cursor;
        public CursorParser(Cursor cursor) {
            m_cursor = cursor;
        }

        public String getString(String column) { return SQuery.parseString(m_cursor, column); }
        public String getString(String column, String whenNull) { return SQuery.parseString(m_cursor, column, whenNull); }

        public int getInt(String column) { return SQuery.parseInt(m_cursor, column); }
        public int getInt(String column, int whenNull) { return SQuery.parseInt(m_cursor, column, whenNull); }

        public long getLong(String column) { return SQuery.parseLong(m_cursor, column); }
        public long getLong(String column, long whenNull) { return SQuery.parseLong(m_cursor, column, whenNull); }

        public float getFloat(String column) { return SQuery.parseFloat(m_cursor, column); }
        public float getFloat(String column, float whenNull) { return SQuery.parseFloat(m_cursor, column, whenNull); }

        public double getDouble(String column) { return SQuery.parseDouble(m_cursor, column); }
        public double getDouble(String column, double whenNull) { return SQuery.parseDouble(m_cursor, column, whenNull); }

        public boolean getBoolean(String column) { return SQuery.parseBoolean(m_cursor, column); }
        public boolean getBoolean(String column, boolean whenNull) { return SQuery.parseBoolean(m_cursor, column, whenNull); }

        public Date getDateFromLong(String column) { return SQuery.parseDateFromLong(m_cursor, column); }
        public Date getDateFromLong(String column, Date whenNull) { return SQuery.parseDateFromLong(m_cursor, column, whenNull); }

        public boolean isNull(String column) {
            final int idx = m_cursor.getColumnIndex(column);
            return m_cursor.isNull(idx);
        }
    }

    public static CursorParser parseWith(Cursor cursor) {
        return new CursorParser(cursor);
    }

    public static String parseString(Cursor cursor, String column) {
        return parseString(cursor, column, null);
    }
    public static String parseString(Cursor cursor, String column, String whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getString(idx);
    }

    public static int parseInt(Cursor cursor, String column) {
        return parseInt(cursor, column, 0);
    }
    public static int parseInt(Cursor cursor, String column, int whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getInt(idx);
    }

    public static long parseLong(Cursor cursor, String column) {
        return parseLong(cursor, column, 0);
    }
    public static long parseLong(Cursor cursor, String column, long whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getLong(idx);
    }

    public static float parseFloat(Cursor cursor, String column) {
        return parseFloat(cursor, column, 0);
    }
    public static float parseFloat(Cursor cursor, String column, float whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getFloat(idx);
    }

    public static double parseDouble(Cursor cursor, String column) {
        return parseDouble(cursor, column, 0);
    }
    public static double parseDouble(Cursor cursor, String column, double whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getDouble(idx);
    }

    public static boolean parseBoolean(Cursor cursor, String column) {
        return parseBoolean(cursor, column, false);
    }
    public static boolean parseBoolean(Cursor cursor, String column, boolean whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;
        return cursor.getInt(idx) != 0;
    }

    public static Date parseDateFromLong(Cursor cursor, String column) {
        return parseDateFromLong(cursor, column, null);
    }
    public static Date parseDateFromLong(Cursor cursor, String column, Date whenNull) {
        final int idx = cursor.getColumnIndex(column);
        if (cursor.isNull(idx)) return whenNull;

        final long value = cursor.getLong(idx);
        return new Date(value);
    }

    public static <T extends DBRowExchanger> void foreach(DBRowExchangerCreator creator, Cursor cursor, SQLiteDataTypes.EachAction<T> action) {
        if (cursor == null) return;

        while (cursor.moveToNext()) {
            final T row = (T)creator.create();
            if (row != null) {
                row.loadFromCursor(cursor);

                if (action != null) {
                    action.execute(row);
                }

            }
        }
    }

    public static <T extends DBRowExchanger> void foreach(final Class<T> dataType, Cursor cursor, SQLiteDataTypes.EachAction<T> action) {
        foreach(new DBRowExchangerCreator() {
            @Override
            public DBRowExchanger create() {
                try {
                    return dataType.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, cursor, action);
    }

}
