package sizuha.library.slib.squery

import android.content.ContentValues
import android.database.Cursor
import sizuha.library.slib.types.SQLiteDataTypes


class PreferenceDB(private val db: SQuery, tableName: String, val keyField: String = "p_name", val valueField: String = "p_value") {

    private val tableDef: TableDefine = TableDefine(tableName)

    init {
        tableDef.addKeyColumn(keyField, SQLiteDataTypes.Text)
        tableDef.addColumn(valueField, SQLiteDataTypes.Text)
    }

    inner class Property(var key: String) : DBRowExchanger {
        var value: String? = null

        override fun loadFromCursor(cursor: Cursor?) {
            key = SQuery.parseString(cursor, keyField)
            value = SQuery.parseString(cursor, valueField)
        }

        override fun toContentValues(): ContentValues {
            val result = ContentValues()
            result.put(keyField, key)
            result.put(valueField, value)
            return result
        }
    }

    fun put(key: String, value: String): Boolean {
        val item = Property(key)
        item.value = value
        return db.from(tableDef).values(item).insertOrUpdate()
    }

    fun getString(key: String): String? {
        return db.from(tableDef)
                .where("key=?", key)
                .selectOne<Property>{ Property(key) }?.value
    }

    fun getString(key: String, default: String): String? {
        return getString(key) ?: default
    }

    fun put(key: String, value: Int): Boolean {
        return put(key, value.toString())
    }

    fun getInt(key: String, default: Int = 0): Int {
        return getString(key)?.toIntOrNull() ?: default
    }

    fun put(key: String, value: Long): Boolean {
        return put(key, value.toString())
    }

    fun getLong(key: String, default: Long = 0): Long {
        return getString(key)?.toLongOrNull() ?: default
    }

    fun put(key: String, value: Float): Boolean {
        return put(key, value.toString())
    }

    fun getFloat(key: String, default: Float = 0.0f): Float {
        return getString(key)?.toFloatOrNull() ?: default
    }

    fun put(key: String, value: Double): Boolean {
        return put(key, value.toString())
    }

    fun getDouble(key: String, default: Double = 0.0): Double {
        return getString(key)?.toDoubleOrNull() ?: default
    }

}