package sizuha.library.slib.squery;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import sizuha.library.slib.types.DBRowExchangerCreator;
import sizuha.library.slib.types.SQLiteDataTypes;

/**
 * SQLiteのDBを対象にするQuery文の作成と実行を手伝うクラス。
 *
 * Usage ex)
 *      QueryBuilder m_db = new SQuery(openDatabaseOpenHelper).from(tableDefine);
 *
 * SELECT:
 *      List<DataType> rows = m_db.where("date >= ?", arg1)
 *      .orderBy("date", false)
 *      .select(DataType.class);
 *
 * UPDATE:
 *      m_db.where("id = ?", arg1).values(data).update();
 *
 * INSERT:
 *      m_db.values(data).insert();
 *
 * DELETE:
 *      m_db.where("id = ?", arg1).delete();
 *
 * INSERT or UPDATE:
 *      m_db.where("id = ?", arg1).values(data).insertOrUpdate();
 *
 */
final public class QueryBuilder {

    protected enum Method {
        SELECT, DELETE, INSERT, UPDATE
    }

    protected final SQLiteDatabase m_db;
    protected final TableDefine m_table;
    protected final String[] m_allColumnNames;

    protected String m_sqlWhere;
    protected String[] m_sqlSelectionArgs;
    protected String m_sqlGroupBy;
    protected String m_sqlOrderBy;
    protected String m_sqlHaving;
    protected ContentValues m_sqlValues;
    protected int m_sqlLimit;
    protected int m_sqlLimitOffset;
    protected boolean m_distinct;


    public QueryBuilder(SQLiteDatabase db, TableDefine table) {
        m_db = db;
        m_table = table;
        m_allColumnNames = m_table.getColumnNames();
    }

    /**
     * SQLの「WHERE」パート。
     * Queryの条件をセットする。
     * @param selection
     * @param selectionArgs
     * @return
     */
    @NotNull
    public QueryBuilder where(String selection, String... selectionArgs) {
        this.m_sqlWhere = selection;
        this.m_sqlSelectionArgs = selectionArgs;
        return this;
    }

    /**
     * SQLの「WHERE」パート。
     * Queryの条件をセットする。
     * @param selection
     * @return
     */
    @NotNull
    public QueryBuilder where(String selection) {
        this.m_sqlWhere = selection;
        this.m_sqlSelectionArgs = null;
        return this;
    }

    /**
     * SQLの「VALUES」パート。
     * 書くデータをセットする。
     * @param values
     * @return
     */
    @NotNull
    public QueryBuilder values(ContentValues values) {
        this.m_sqlValues = values;
        return this;
    }

    /**
     * SQLの「VALUES」パート。
     * DBRowExchanger interfaceを持つオブジェクトから自動でContentValues型に変換する。
     * @param data
     * @return
     */
    @NotNull
    public QueryBuilder values(DBRowExchanger data) {
        this.m_sqlValues = data.toContentValues();
        return this;
    }

    /**
     * SQLの「GROUP BY」パート。
     * @param groupBy
     * @return
     */
    @NotNull
    public QueryBuilder groupBy(String groupBy) {
        this.m_sqlGroupBy = groupBy;
        return this;
    }

    public QueryBuilder having(String having) {
        this.m_sqlHaving = having;
        return this;
    }

    /**
     * SQLの「ORDER BY」パート。
     * 結果をソート。
     * 複数設定可能。
     * @param field ソートするフィールド(column)
     * @return
     */
    @NotNull
    public QueryBuilder orderBy(String field) {
        return orderBy(field, true);
    }

    /**
     * SQLの「ORDER BY」パート。
     * 結果をソート。
     * 複数設定可能。
     * @param field ソートするフィールド(column)
     * @param asc true = 低から高、false = 高から低
     * @return
     */
    @NotNull
    public QueryBuilder orderBy(String field, boolean asc) {
        if (m_sqlOrderBy == null) m_sqlOrderBy = "";

        m_sqlOrderBy += (m_sqlOrderBy.isEmpty() ? "" : ", ") + field + (asc ? " ASC" : " DESC");
        return this;
    }

    /**
     * SQLの「ORDER BY」パート。
     * 以前のorderBy()の設定は無効になる。
     * @param orderBy ソート条件文
     * @return
     */
    @NotNull
    public QueryBuilder orderBy_raw(String orderBy) {
        this.m_sqlOrderBy = orderBy;
        return this;
    }

    /**
     * 返されるrowの数を制限
     *
     * @param rowCount 行数
     * @return
     */
    @NotNull
    public QueryBuilder limit(int rowCount) {
        return limit(rowCount, 0);
    }

    /**
     * 返されるrowの数を制限
     *
     * @param rowCount 行数
     * @param offset 開始位置
     * @return
     */
    @NotNull
    public QueryBuilder limit(int rowCount, int offset) {
        this.m_sqlLimit = rowCount;
        this.m_sqlLimitOffset = offset;
        return this;
    }


    @NotNull
    final public QueryBuilder distinct() {
        return distinct(true);
    }

    @NotNull
    public QueryBuilder distinct(boolean onOff) {
        m_distinct = onOff;
        return this;
    }


    //---- execute Query -----------------------------

    /**
     * SQLの「SELECT」を実行して、Cursorオブジェクトを返す。
     * 使用後Cursorオブジェクトは必ずCloseすること。
     *
     * @return 結果をCursorオブジェクトで返す
     */
    public Cursor query() {
        return query(m_allColumnNames);
    }

    /**
     * SQLの「SELECT」を実行して、Cursorオブジェクトを返す。
     * 使用後Cursorオブジェクトは必ずCloseすること。
     *
     * @param columns 結果で返されるcolumn達
     * @return 結果をCursorオブジェクトで返す
     */
    public Cursor query(String... columns) {
        String limitStr = null;
        if (m_sqlLimit > 0) {
            limitStr = m_sqlLimitOffset > 0 ? (m_sqlLimitOffset+","+m_sqlLimit) : String.valueOf(m_sqlLimit);
        }

        final Cursor cursor = m_db.query(m_distinct, m_table.getTableName(), columns,
                m_sqlWhere, m_sqlSelectionArgs, m_sqlGroupBy, m_sqlHaving, m_sqlOrderBy, limitStr);

        return cursor;
    }

    /**
     * SQLの「SELECT」を実行した後、結果として返されたrowの数だけを返す
     *
     * @return rowの数
     */
    public int count() {
        int result = 0;

        final Cursor cursor = query();
        if (cursor != null) {
            result = cursor.getCount();
            cursor.close();
        }

        return result;
    }

    @NotNull
    public <T extends DBRowExchanger> List<T> select(DBRowExchangerCreator creator, String... columns) {
        if (columns.length < 1) {
            return selectForeach(creator, null);
        }

        final Cursor cursor = query(columns);
        final int size = cursor.getCount();
        final List<T> result = new ArrayList<>(size);

        try {
            SQuery.foreach(creator, cursor, new SQLiteDataTypes.EachAction<DBRowExchanger>() {
                @Override
                public void execute(DBRowExchanger dbRowExchanger) {
                    result.add((T) dbRowExchanger);
                }
            });
        }
        finally {
            cursor.close();
        }
        return result;
    }


    /**
     * SQLの「SELECT」を実行する。
     *
     * @param dataType DBRowExchanger interfaceを持つClass
     * @param columns 省略すると全カラムを対象にする
     * @return rowをdataType型のオブジェクトに変換してArrayListで返す
     */
    public <T extends DBRowExchanger> List<T> select(final Class<T> dataType, String... columns) {
        return select(new DBRowExchangerCreator() {
            @Override
            public DBRowExchanger create() {
                try {
                    return dataType.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, columns);
    }

    @NotNull
    public <T extends DBRowExchanger> List<T> selectForeach(DBRowExchangerCreator creator, SQLiteDataTypes.EachAction<T> action) {
        final Cursor cursor = query();
        if (cursor == null) return new ArrayList<>(0);

        final List<T> result = new ArrayList<>(cursor.getCount());
        try {
            while (cursor.moveToNext()) {
                final T row = (T) creator.create();
                if (row != null) {
                    row.loadFromCursor(cursor);
                    result.add(row);

                    if (action != null) {
                        action.execute(row);
                    }
                }
            }
        }
        finally {
            cursor.close();
        }

        return result;
    }


    public <T extends DBRowExchanger> List<T> selectForeach(final Class<T> dataType, SQLiteDataTypes.EachAction<T> action) {
        return selectForeach(new DBRowExchangerCreator() {
            @Override
            public DBRowExchanger create() {
                try {
                    return dataType.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, action);
    }


    public <T extends DBRowExchanger> T selectOne(DBRowExchangerCreator creator) {
        limit(1);
        final List<T> rows = select(creator);

        if (!rows.isEmpty())return rows.get(0);
        return null;
    }

    public <T extends DBRowExchanger> T selectOne(Class<T> dataType) {
        limit(1);
        final List<T> rows = select(dataType);

        if (rows != null && !rows.isEmpty()) return rows.get(0);
        return null;
    }


    public <T extends DBRowExchanger> void foreachWithCreator(DBRowExchangerCreator creator, String[] columns, SQLiteDataTypes.EachAction<T> action) {
        final Cursor cursor = columns != null && columns.length > 0 ? query(columns) : query();
        try {
            SQuery.foreach(creator, cursor, action);
        }
        finally {
            cursor.close();
        }
    }

    public <T extends DBRowExchanger> void foreachWithCreator(DBRowExchangerCreator creator, SQLiteDataTypes.EachAction<T> action) {
        foreachWithCreator(creator, null, action);
    }

    public <T extends DBRowExchanger> void foreach(final Class<T> dataType, String[] columns, SQLiteDataTypes.EachAction<T> action) {
        foreachWithCreator(new DBRowExchangerCreator() {
            @Override
            public DBRowExchanger create() {
                try {
                    return dataType.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, columns, action);
    }

    public <T extends DBRowExchanger> void foreach(Class<T> dataType, SQLiteDataTypes.EachAction<T> action) {
        foreach(dataType, null, action);
    }


    /**
     * SQLの「INSERT」を実行する。
     * values()で先にデータをセットする必要がある。
     * @return 成功したらtrue
     */
    public boolean insert() {
        final List<TableDefine.ColumnDefine> cols = m_table.getColumns();
        for (TableDefine.ColumnDefine col : cols) {
            if (col.autoIncrement) {
                m_sqlValues.remove(col.name);
            }
        }
        return execute(Method.INSERT);
    }

    /**
     * SQLの「UPDATE」を実行する。
     * 先にwhere()とvalues()をセットしておく必要がある。
     * @return 成功したらtrue
     */
    public boolean update() {
        final boolean empty_where = m_sqlWhere == null || m_sqlWhere.isEmpty();
        final List<TableDefine.ColumnDefine> cols = m_table.getColumns();
        List<String> where_args = null;

        if (empty_where) {
            m_sqlWhere = "";
            where_args = new ArrayList<>(cols.size());
        }

        final StringBuilder qStr = new StringBuilder(m_sqlWhere);
        boolean first = true;

        for (TableDefine.ColumnDefine col : cols) {
            if (empty_where && col.columnType == TableDefine.ColumnType.PRIMARY_KEY) {
                if (first) first = false;
                else qStr.append(" and ");

                qStr.append(col.name).append(" = ?");
                where_args.add(m_sqlValues.getAsString(col.name));
            }

            if (col.autoIncrement) {
                m_sqlValues.remove(col.name);
            }
        }

        if (empty_where) {
            m_sqlSelectionArgs = new String[where_args.size()];
            where_args.toArray(m_sqlSelectionArgs);
        }

        m_sqlWhere = qStr.toString();
        return execute(Method.UPDATE);
    }

    /**
     * Tableからrowを削除する
     */
    public void delete() {
        execute(Method.DELETE);
    }

    /**
     * whereの条件に合うデータがある場合はUPDATE、ない場合はINSERTを試す
     * @return 成功したらtrue
     */
    public boolean insertOrUpdate() {
        return insert() || update();

    }

    private boolean execute(Method method) {
        boolean result = true;

        switch (method) {
            case SELECT:
                final Cursor cursor = query();
                if (cursor != null) {
                    cursor.close();
                    result = true;
                }
                else {
                    result = false;
                }
                break;

            case INSERT:
                try {
                    m_db.insertOrThrow(m_table.getTableName(), null, m_sqlValues);
                    result = true;
                }
                catch (SQLException e) {
                    if (SQuery.isDebugLogEnabled()) {
                        Log.e(SQuery.TAG, e.toString());
                    }
                    result = false;
                }
                break;

            case UPDATE:
                if (SQuery.isDebugLogEnabled()) {
                    Log.d(SQuery.TAG, "try update with ==> " + m_sqlValues.toString());
                }

                final int count = m_db.update(m_table.getTableName(), m_sqlValues, m_sqlWhere, m_sqlSelectionArgs);
                result = count > 0;
                break;

            case DELETE:
                m_db.delete(m_table.getTableName(), m_sqlWhere, m_sqlSelectionArgs);
                break;

            default:
                break;
        }

        return result;
    }

    public void reset() {
        m_sqlWhere = null;
        m_sqlValues = null;
        m_sqlGroupBy = null;
        m_sqlLimit = 0;
        m_sqlLimitOffset = 0;
        m_sqlSelectionArgs = null;
        m_sqlOrderBy = null;
    }

}
