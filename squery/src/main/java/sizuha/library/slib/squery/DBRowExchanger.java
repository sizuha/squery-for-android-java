package sizuha.library.slib.squery;

import android.content.ContentValues;
import android.database.Cursor;

public interface DBRowExchanger {

    /**
     * DBのRowからデータをimportする。
     * @param cursor
     */
    void loadFromCursor(Cursor cursor);

    /**
     * Queryに使う為のContentValuesオブジェクトを作成する。
     * @return ContentValues
     */
    ContentValues toContentValues();

}
