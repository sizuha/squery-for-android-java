package sizuha.library.slib.squery;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

public abstract class DatabaseOpenHelper extends SQLiteOpenHelper {

    private List<TableDefine> m_tableDefines;
    public List<TableDefine> getTableDefines() {
        return m_tableDefines;
    }

    public DatabaseOpenHelper(Context context, String name, int version, List<TableDefine> tableDefines) {
        super(context, name, null, version);

        this.m_tableDefines = tableDefines;
    }

    protected void recreateTable(SQLiteDatabase db, TableDefine table) {
        db.execSQL("DROP TABLE " + table.getTableName());
        TableMaker.createTable(db, table);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        TableMaker.createTables(db, m_tableDefines);
    }

    public SQuery openQuery() {
        return new SQuery(this);
    }

}
