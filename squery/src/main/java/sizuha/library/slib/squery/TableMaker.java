package sizuha.library.slib.squery;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import sizuha.library.slib.types.SQLiteDataTypes;

public class TableMaker {

    public static void createTables(SQLiteDatabase db, List<TableDefine> defines) {
        for (TableDefine t: defines) {
            createTable(db, t);
        }
    }

    public static void createTable(SQLiteDatabase db, TableDefine define) {
        final StringBuilder sql_builder = new StringBuilder();
        sql_builder.append("CREATE TABLE " + define.getTableName() + " (");

        final List<TableDefine.ColumnDefine> columns = define.getColumns();


        final List<TableDefine.ColumnDefine> primary_keys = new ArrayList<>();
        for (TableDefine.ColumnDefine col: columns) {
            if (col.columnType == TableDefine.ColumnType.PRIMARY_KEY) {
                primary_keys.add(col);
            }
        }
        final boolean is_single_pk = primary_keys.size() == 1;

        boolean isFirst = true;
        for (TableDefine.ColumnDefine col: columns) {
            if (isFirst) {
                isFirst = false;
            }
            else {
                sql_builder.append(",");
            }

            sql_builder.append(col.name + " " + col.dataType.toString());

            switch (col.columnType) {
                case PRIMARY_KEY:
                    if (is_single_pk) {
                        sql_builder.append(" PRIMARY KEY");

                        if (col.autoIncrement && col.dataType == SQLiteDataTypes.Integer) {
                            sql_builder.append(" AUTOINCREMENT");
                        }
                        else if (!col.defaultOrderbyAsc) sql_builder.append(" DESC");
                    }
                    break;

                case UNIQUE:
                    sql_builder.append(" UNIQUE");
                    break;

                case NOT_NULL:
                    sql_builder.append(" NOT NULL");
                    break;
            }
        }

        if (!is_single_pk) {
            isFirst = true;
            sql_builder.append(", PRIMARY KEY(");
            for (TableDefine.ColumnDefine pk : primary_keys) {
                if (isFirst) isFirst = false;
                else sql_builder.append(",");

                sql_builder.append(pk.name);
                if (!pk.defaultOrderbyAsc) sql_builder.append(" DESC");
            }
            sql_builder.append(")");
        }

        sql_builder.append(");");
        db.execSQL(sql_builder.toString());
    }

}
