package sizuha.library.slib.squery;


import org.jetbrains.annotations.NotNull;

import sizuha.library.slib.types.SQLiteDataTypes;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * SQLiteのDB Tableを定義するクラス。
 */
public class TableDefine {

    private String name;


    public enum ColumnType {
        NORMAL, PRIMARY_KEY, UNIQUE, NOT_NULL
    }

    public static class ColumnDefine {
        public String name;
        public SQLiteDataTypes dataType;
        public ColumnType columnType;
        public boolean autoIncrement;
        public boolean defaultOrderbyAsc = true;
    }

    private List<ColumnDefine> columns;


    public TableDefine() {
        this.columns = new LinkedList<>();
    }
    public TableDefine(int columns) {
        this.columns = new ArrayList<>(columns);
    }
    public TableDefine(String tableName) {
        this.name = tableName;
        this.columns = new LinkedList<>();
    }
    public TableDefine(String tableName, int columns) {
        this.name = tableName;
        this.columns = new ArrayList<>(columns);
    }


    public TableDefine setTableName(String name) {
        this.name = name;
        return this;
    }
    public String getTableName() {
        return this.name;
    }

    public void clearColumns() {
        columns.clear();
    }

    @NotNull
    public TableDefine addColumn(String name, SQLiteDataTypes dataType) {
        addColumn(name, dataType, false);
        return this;
    }

    @NotNull
    public TableDefine addColumn(String name, SQLiteDataTypes dataType, boolean notNull) {
        addColumn(name, dataType, notNull ? ColumnType.NOT_NULL : ColumnType.NORMAL);
        return this;
    }

    @NotNull
    public TableDefine addUniqueColumn(String name, SQLiteDataTypes dataType) {
        addColumn(name, dataType, ColumnType.UNIQUE);
        return this;
    }

    @NotNull
    public TableDefine addIdKeyColumn(String name) {
        addColumn(name, SQLiteDataTypes.Integer, ColumnType.PRIMARY_KEY).autoIncrement = true;
        return this;
    }

    @NotNull
    public TableDefine addKeyColumn(String name, SQLiteDataTypes dataType) {
        return addKeyColumn(name, dataType, true);
    }

    @NotNull
    public TableDefine addKeyColumn(String name, SQLiteDataTypes dataType, boolean orderByAsc) {
        addColumn(name, dataType, ColumnType.PRIMARY_KEY, orderByAsc);
        return this;
    }

    @NotNull
    public ColumnDefine addColumn(String name, SQLiteDataTypes dataType, ColumnType columnType) {
        return addColumn(name, dataType, columnType, true);
    }

    @NotNull
    protected ColumnDefine addColumn(String name, SQLiteDataTypes dataType, ColumnType columnType, boolean keyOrderByAsc) {
        final ColumnDefine col = new ColumnDefine();
        col.name = name;
        col.dataType = dataType;
        col.columnType = columnType;
        col.autoIncrement = false;
        col.defaultOrderbyAsc = keyOrderByAsc;

        columns.add(col);
        return col;
    }

    public List<ColumnDefine> getColumns() { return columns; }

    @NotNull
    public String[] getColumnNames() {
        return getColumnNames(false);
    }

    @NotNull
    public String[] getColumnNames(boolean withTableName) {
        final String[] result = new String[columns.size()];
        final String table_name = withTableName ? getTableName() + "." : "";

        int i = 0;
        for (ColumnDefine col: columns) {
            result[i++] = table_name + col.name;
        }

        return result;
    }

    @NotNull
    public List<ColumnDefine> getPrimaryKeys() {
        final List<ColumnDefine> result = new ArrayList<>(columns.size());

        for (ColumnDefine c : columns) {
            if (c.columnType == ColumnType.PRIMARY_KEY) {
                result.add(c);
            }
        }

        return result;
    }

}
